import pytest
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from pages.main_page import MainPage
from pages.regional_settings_page import RegionalSettingsPage
from pages.cart_page import CartPage
from mysql.connector import connect
from services.sql_service import SqlService


@pytest.fixture()
def chromedriver():
    options = Options()
    options.add_argument('--headless')
    return webdriver.Chrome(ChromeDriverManager().install(), options=options)


@pytest.fixture()
def regional_settings(chromedriver):
    return RegionalSettingsPage(chromedriver)


@pytest.fixture()
def main_page(chromedriver):
    return MainPage(chromedriver)


@pytest.fixture()
def cart_page(chromedriver):
    return CartPage(chromedriver)


@pytest.fixture()
def setup_for_change_country_and_currency(chromedriver, regional_settings, main_page):
    main_page.navigate()
    main_page.click_regional_settings_button()
    regional_settings.change_country_and_currency_to_proper()


@pytest.fixture()
def setup_for_quantity_and_price_of_ducks(chromedriver, main_page, cart_page):
    main_page.navigate()
    main_page.login_to_account()
    main_page.form_an_order_and_go_to_cart()


@pytest.fixture()
def confirm_order(cart_page):
    yield
    cart_page.click_confirm_order()


@pytest.fixture()
def connection_to_data_base():
    connection = connect(host="localhost",
                         user="root",
                         password="",
                         database='litecart'
                         )
    yield connection
    connection.close()


@pytest.fixture()
def sql_service(connection_to_data_base):
    return SqlService(connection_to_data_base)


@pytest.fixture()
def chromedriver_quit(chromedriver):
    yield chromedriver
    chromedriver.quit()
