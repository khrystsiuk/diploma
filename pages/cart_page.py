from pages.base_page import BasePage


class CartPage(BasePage):

    QUANTITY_LOCATOR = "//td[contains(@style,'text-align: center')]"
    FINAL_PRICE_LOCATOR = "//td[@class='sum']"
    BILL_LOCATOR = "//strong[contains(text(),'$')]"
    CONFIRM_ORDER_LOCATOR = "//button[@name='confirm_order']"

    def check_quantity_of_ducks(self):
        return int(self.get_element_text(self.QUANTITY_LOCATOR))

    def check_price_of_ducks(self):
        return self.get_element_text(self.FINAL_PRICE_LOCATOR)

    def check_bill(self):
        return self.get_element_text(self.BILL_LOCATOR)

    def click_confirm_order(self):
        self.click(self.CONFIRM_ORDER_LOCATOR)
