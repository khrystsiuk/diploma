from pages.base_page import BasePage


class MainPage(BasePage):

    URL = 'http://localhost/litecart/en/'

    EMAIL_INPUT_LOCATOR = "//input[@name='email']"
    PASSWORD_INPUT_LOCATOR = "//input[@name='password']"
    LOGIN_BUTTON_LOCATOR = "//button[@name='login']"

    DUCK_LOCATOR = "//a[@title='{} Duck'][@class='link']"
    INPUT_QUANTITY_LOCATOR = "//input[@name='quantity']"
    BUTTON_ADD_TO_CART_LOCATOR = "//button[@name='add_cart_product']"
    CHECKOUT_CART_LOCATOR = "//a[contains(text(),'Checkout')]"
    QUANTITY_CHECKOUT_CART_LOCATOR = "//span[text()='{}']"

    REGIONAL_SETTINGS_BUTTON_LOCATOR = "//li/a[@href='http://localhost/litecart/en/regional_settings']"

    def navigate(self):
        self.url_open(self.URL)

    def input_user_email(self, email_text):
        self.send_text(self.EMAIL_INPUT_LOCATOR, email_text)

    def input_user_password(self, password_text):
        self.send_text(self.PASSWORD_INPUT_LOCATOR, password_text)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def click_duck(self, duck_name):
        self.click(self.DUCK_LOCATOR.format(duck_name))

    def click_input_quantity(self):
        self.click(self.INPUT_QUANTITY_LOCATOR)

    def clear_input_quantity(self):
        self.clear(self.INPUT_QUANTITY_LOCATOR)

    def enter_number_of_ducks(self, ducks_number):
        self.send_text(self.INPUT_QUANTITY_LOCATOR, ducks_number)

    def click_button_add_to_cart(self):
        self.click(self.BUTTON_ADD_TO_CART_LOCATOR)

    def click_cart_checkout(self):
        self.click(self.CHECKOUT_CART_LOCATOR)

    def login_to_account(self):
        self.input_user_email('123@mail.com')
        self.input_user_password('text1')
        self.click_login_button()

    def form_an_order_and_go_to_cart(self):
        self.click_duck("Blue")
        self.click_input_quantity()
        self.clear_input_quantity()
        self.enter_number_of_ducks("3")
        self.click_button_add_to_cart()
        self.wait_for_element_presence(self.QUANTITY_CHECKOUT_CART_LOCATOR.format("3"))
        self.click_cart_checkout()

    def click_regional_settings_button(self):
        self.click(self.REGIONAL_SETTINGS_BUTTON_LOCATOR)
