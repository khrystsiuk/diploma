from pages.base_page import BasePage


class RegionalSettingsPage(BasePage):

    SELECT_COUNTRY_BUTTON_LOCATOR = "//span[@class='select2-selection__rendered']"
    SELECT_CURRENCY_BUTTON_LOCATOR = "//select[@name='currency_code']"
    PROPER_COUNTRY_LOCATOR = "//li[contains(text(),'Ukraine')]"
    PROPER_CURRENCY_LOCATOR = "//option[@value='EUR']"
    SAVE_BUTTON_LOCATOR = "//button[@name='save']"

    def choose_country(self):
        self.click(self.SELECT_COUNTRY_BUTTON_LOCATOR)
        self.click(self.PROPER_COUNTRY_LOCATOR)

    def choose_currency(self):
        self.click(self.SELECT_CURRENCY_BUTTON_LOCATOR)
        self.click(self.PROPER_CURRENCY_LOCATOR)

    def save_changes(self):
        self.click(self.SAVE_BUTTON_LOCATOR)

    def change_country_and_currency_to_proper(self):
        self.choose_country()
        self.choose_currency()
        self.save_changes()
