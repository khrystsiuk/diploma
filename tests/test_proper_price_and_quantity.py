import pytest
import allure


@pytest.mark.usefixtures("setup_for_quantity_and_price_of_ducks")
class TestProperPriceAndQuantity:

    @allure.description("This test checks the quantity of goods added and confirms it to the cart page")
    def test_quantity_of_ducks(self, cart_page):
        quantity_on_cart_page = cart_page.check_quantity_of_ducks()

        assert quantity_on_cart_page == 3, f"Quantity of ducks should be equel to 3"

    @allure.description("This test checks the final price of goods and confirms the order")
    @pytest.mark.usefixtures("confirm_order")
    def test_price_of_ducks(self, cart_page):
        final_price_of_ducks = cart_page.check_price_of_ducks()
        total_bill = cart_page.check_bill()

        assert final_price_of_ducks == total_bill, f"Expected price of ducks {final_price_of_ducks}, got {total_bill}"
