import pytest
import allure


@pytest.mark.usefixtures("setup_for_change_country_and_currency")
class TestRegionalSettings:

    @allure.description("This test changes currency to proper value and checks if it changed on the UI")
    def test_currency_is_valid(self, regional_settings):
        proper_currency_value = "EUR"
        currency_value = regional_settings.get_element_text("//div[@class='currency']")
        assert proper_currency_value == currency_value, f"expected {proper_currency_value}, got {currency_value}"

    @allure.description("This test changes country to proper value and checks if it changed on the UI")
    def test_country_is_valid(self, regional_settings):
        proper_country_value = "Ukraine"
        country_value = regional_settings.get_element_text("//div[@class='country']")
        assert proper_country_value == country_value, f"expected {proper_country_value}, got {country_value}"
